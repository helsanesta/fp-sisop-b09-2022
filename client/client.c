#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct allowed{
	char name[10000];
	char pw[10000];
};

int cekAllowed(char *username, char *pw){
	FILE *fp;
	struct allowed user;
	int id,count=0;
	fp=fopen("../database/databases/user.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, username)==0){
			if(strcmp(user.pw, pw)==0){
				count=1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);

	if(count==0){
		printf("Permission Denied!!\n");
		return 0;
	}else{
		return 1;
	}
	
}

int main(int argc, char *argv[]){
	
	int allowed=0;
	int id_user = geteuid();
	char database_used[1000];
	if(geteuid() == 0){
		
		allowed=1;
	}else{
		int id = geteuid();
		
		allowed = cekAllowed(argv[2],argv[4]);
	}
	if(allowed==0){
		return 0;
	}
	int clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[32000];

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(clientSocket < 0){
		printf("---Connection error.\n");
		exit(1);
	}
	printf("+++Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("---Connection error.\n");
		exit(1);
	}
	printf("Connected.\n");
	while(1){
		printf("Client: \t");
		char input[10000];
		char copyinput[10000];
		char command[100][10000];
		char *temp;
		int i=0;  
		scanf(" %[^\n]s", input);
		strcpy(copyinput, input);
		
		temp = strtok(input, " ");
		while( temp != NULL ) {
			strcpy(command[i], temp);
			i++;
			temp = strtok(NULL, " ");
		}

		int wrongCommand = 0;
		
		if(strcmp(command[0], "CREATE")==0){
			if(strcmp(command[1], "USER")==0 && strcmp(command[3], "IDENTIFIED")==0 && strcmp(command[4], "BY")==0){
				snprintf(buffer, sizeof buffer, "User:%s:%s:%d", command[2], command[5], id_user);
				
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(command[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "Database:%s:%s:%d", command[2], argv[2], id_user);
				
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		}else if(strcmp(command[0], "GRANT")==0 && strcmp(command[1], "PERMISSION")==0 && strcmp(command[3], "INTO")==0){
			snprintf(buffer, sizeof buffer, "Permission:%s:%s:%d", command[2],command[4], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(command[0], "USE")==0){
			snprintf(buffer, sizeof buffer, "useDatabase:%s:%s:%d", command[1], argv[2], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(command[0], "cekCurrentDatabase")==0){
			snprintf(buffer, sizeof buffer, "%s", command[0]);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(command[0], ":exit")!=0){
			wrongCommand = 1;
			char warning[] = "Wrong Command";
			send(clientSocket, warning, strlen(warning), 0);
			
		}

		if(wrongCommand != 1){
			char namaSender[10000];
			if(id_user == 0){
				strcpy(namaSender, "root");
			}else{
				strcpy(namaSender, argv[2]);
			}
			
		}

		if(strcmp(command[0], ":exit") == 0){
			send(clientSocket, command[0], strlen(command[0]), 0);
			close(clientSocket);
			printf("---Disconnected.\n");
			exit(1);
		}
		bzero(buffer, sizeof(buffer));
		if(recv(clientSocket, buffer, 1024, 0) < 0){
			printf("---Receiving data error.\n");
		}else{
			printf("Server: \t%s\n", buffer);
		}
	}

	return 0;
}
