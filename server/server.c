#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080

struct allowed{
	char n[10000];
	char p[10000];
};

struct allow_to_database{
	char database[100];
	char name[100];
};

struct table{
	int kolom;
	char tipe[100][100];
	char data[100][100];
};

void buatUser(char *name, char *pass);

int cekUser(char *username);

void insertPermission(char *nama, char *database);

int cekAllowedDatabase(char *nama, char *database);

int main(){

	int sockfd, ret;
	struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("---Connection error.\n");
		exit(1);
	}
	printf("+++Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		exit(1);
	}

	if(listen(sockfd, 10) == 0){
		printf("+++Tersambung....\n");
	}else{
		exit(1);
	}


	while(1){
		newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
		if(newSocket < 0){
			exit(1);
		}
		printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

		if((childpid = fork()) == 0){
			close(sockfd);

			while(1){
				recv(newSocket, buffer, 1024, 0);
				printf("buffer %s\n", buffer);

				//kode lain
				char *temp;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char command[100][100];
				temp = strtok(buffercopy, ":");
				int i=0;
				char database_used[1000];
				while( temp != NULL ) {
					strcpy(command[i], temp);
					
					i++;
					temp = strtok(NULL, ":");
				}
				if(strcmp(command[0], "User")==0){
					if(strcmp(command[3], "0")==0){
						buatUser(command[1], command[2]);
					}else{
						char warn[] = "Permission Denied!!";
						send(newSocket, warn, strlen(warn), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "Permission")==0){
					if(strcmp(command[3], "0")==0){
						int exist = cekUser(command[2]);
						if(exist == 1){
							insertPermission(command[2], command[1]);
						}else{
							char warn[] = "User Tidak Ditemukan";
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}
					}else{
						char warn[] = "Permission Denied!!";
						send(newSocket, warn, strlen(warn), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(command[0], "Database")==0){
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", command[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", lokasi, command[2], command[1]);
					mkdir(lokasi,0777);
					insertPermission(command[2], command[1]);
				}else if(strcmp(command[0], "useDatabase") == 0){
					if(strcmp(command[3], "0") != 0){
						int allowed = cekAllowedDatabase(command[2], command[1]);
						if(allowed != 1){
							char warn[] = "Access to database : Permission Denied!!";
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(database_used, command[1], sizeof(command[1]));
							char warn[] = "Access_database : Allowed";
							printf("database_used = %s\n", database_used);
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
				}else if(strcmp(command[0], "cekCurrentDatabase")==0){
					if(database_used[0] == '\0'){
						strcpy(database_used, "Database not selected");
					}
					send(newSocket, database_used, strlen(database_used), 0);
					bzero(buffer, sizeof(buffer));
				}if(strcmp(buffer, ":exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}else{
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}

	close(newSocket);
	return 0;
}

void buatUser(char *name, char *pass){
	struct allowed user;
	strcpy(user.n, name);
	strcpy(user.p, pass);
	printf("%s %s\n", user.n, user.p);

	FILE *fp;
	fp=fopen("databases/newUser.dat","ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekUser(char *username ){
	FILE *fp;
	struct allowed user;
	int id,count=0;
	fp=fopen("../database/databases/newUser.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		
		if(strcmp(user.n, username)==0){
			return 1;
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);

	return 0;
	
}

void insertPermission(char *nama, char *database){
	struct allow_to_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	
	FILE *fp;
	fp=fopen("databases/permission.dat","ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekAllowedDatabase(char *nama, char *database){
	FILE *fp;
	struct allow_to_database user;
	int id,found=0;
	printf("nama = %s  database = %s", nama, database);
	fp=fopen("../database/databases/permission.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, nama)==0){
			
			if(strcmp(user.database, database)==0){
				return 1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}